# Shopping basket

[![build status](https://gitlab.com/joaopintojeronimo/shopping-basket/badges/master/build.svg)](https://gitlab.com/joaopintojeronimo/shopping-basket/pipelines)
[![coverage report](https://gitlab.com/joaopintojeronimo/shopping-basket/badges/master/coverage.svg)](https://gitlab.com/joaopintojeronimo/shopping-basket/pipelines)

Shopping basket allows a user to shop for Milk, Bread and Butter, using
a terminal!

[![asciicast](https://asciinema.org/a/enl54dwl9n25p5jaequsj895k.png)](https://asciinema.org/a/enl54dwl9n25p5jaequsj895k)

## Some notes

- In this readme all the `yarn` commands can be replaced by `npm` if need be;
- The Decimal.js library is used for aritmetic because doing floating number calculations
in javascript is a [bad idea](http://blog.chewxy.com/2014/02/24/what-every-javascript-developer-should-know-about-floating-point-numbers/).
In retrospective I should have written this in python or anything else.
- The checkout logic is on `checkout.js`, promotion applying logic is on `promotions.js`,
and the different products available are listed on `products.js`. It should be possible to
easily add more promotions and products and the whole thing will work. CLI presentation logic
is on `bin/shop-cli.js`.
- Because this uses a couple of ES2015 features like templates, spread operators and more,
it must be run in at least **Node v6**.

## Running

If you install globally you should have a `shop` command somewhere in your `$PATH`,
If you do not want to install this globally a local install a and `./bin/shop-cli.js`
or `yarn run shop` will work.

## Installing

    yarn install

## Tests

    yarn test

## Coverage

    yarn run test:coverage

For a browser based report,

    open coverage/lcov-report/index.html

## Linting

    yarn run lint

## Objects and extending

## Shopping basket

A shopping basket is an object of the following format:

```
{
  finalPrice: Decimal,
  totalPrice: Decimal,
  discountedValue: Decimal,
  promotionsApplied: []Promotions,
  products: []Products,
}
```

### Promotions

To add a promotion, just add promotion object to `promotions.js`. A promotions
object must have a name and a policy function (takes a shopping basket
as an argument, returns a new shopping basket with promotions applied):

```
{
  name: string,
  policy: (Basket)=>Basket
}
```

### Products

To add a product, just add a product object to `products.js`. A product object must
have a name and a price, a promotion description is optional.

```
{
  name: string,
  price: Decimal,
  promotion: string?,
}
```
