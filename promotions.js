const Decimal = require('decimal.js');
const clone = require('lodash.clone');
const products = require('./products');

module.exports = [
  {
    name: 'Buy 2 Butter and get a Bread at 50% off', // for every two butters, get a bread at 50% off
    policy: (oldBasket) => {
      const basket = clone(oldBasket); // so it's fine to mutate
      const buttersInBasket = basket
        .products
        .filter(product => product.name === 'Butter')
        .length;
      const breadsInBasket = basket
        .products
        .filter(product => product.name === 'Bread')
        .length;

      const numberOfPossibleBreadDiscounts = Math.floor(buttersInBasket / 2);
      if (numberOfPossibleBreadDiscounts > 0 && breadsInBasket > 0) {
        const priceOfBread = products
          .filter(product => product.name === 'Bread')[0].price;

        basket.promotionsApplied = (basket.promotionsApplied || [])
          .concat(['every 2 butters => oneBread @ 50%']);

        const discountedBreads = breadsInBasket > numberOfPossibleBreadDiscounts ?
          numberOfPossibleBreadDiscounts
          :
          breadsInBasket;
        basket.discountedValue = (basket.discountedValue || new Decimal(0))
          .add(priceOfBread.div(2).mul(discountedBreads));
      }

      return basket;
    },
  },
  {
    name: 'Buy 3 Milk and get the 4th milk for free',
    policy: (oldBasket) => {
      const basket = clone(oldBasket); // so it's fine to mutate
      const milksInBasket = basket
        .products
        .filter(product => product.name === 'Milk');
      const freeMilks = Math.floor(milksInBasket.length / 4);
      if (freeMilks > 0) {
        const priceOfMilk = products
          .filter(product => product.name === 'Milk')[0].price;

        basket.promotionsApplied = (basket.promotionsApplied || [])
          .concat(['3 milks => 4th milk free']);

        basket.discountedValue = (basket.discountedValue || new Decimal(0))
          .add(priceOfMilk.mul(freeMilks));
      }
      return basket;
    },
  },
];
