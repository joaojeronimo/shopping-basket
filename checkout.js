const Decimal = require('decimal.js');
const promotions = require('./promotions');

function makeBasket() {
  return {
    finalPrice: new Decimal(0),
    totalPrice: new Decimal(0),
    discountedValue: new Decimal(0),
    promotionsApplied: [],
    products: [],
  };
}

function applyPromotions(initialBasket) {
  return promotions
    .reduce((basket, promotion) => promotion.policy(basket), initialBasket);
}

// accepts an array of products,
// returns the basket
function checkout(products) {
  const totalPrice = products.reduce((total, product) =>
    total.add(product.price), new Decimal(0));

  const basket = Object.assign(makeBasket(), {
    totalPrice,
    products,
  });

  const basketWithPromotionsApplied = applyPromotions(basket);

  basketWithPromotionsApplied.finalPrice =
    basketWithPromotionsApplied
      .totalPrice
      .sub(basketWithPromotionsApplied.discountedValue);

  return basketWithPromotionsApplied;
}

module.exports = checkout;
